functor DuplicateOrdered (Elem : ORDERED) : ORDERED =
struct
  type t = Elem.t * Elem.t
  fun eq ((l, r), (l', r')) =
    Elem.eq (l, l') andalso Elem.eq (r, r')

  fun compare ((l, r), (l', r')) =
    case Elem.compare (l, l') of
        EQUAL => Elem.compare (r, r')
      | r => r
end


functor HOMatch(A : ABT_APPLY) =
struct
  structure A = AbtUtil(A)
  open A
  infix $ \ $$

  type t = A.t
  type var = Variable.t

  structure Solution = SplayDict(structure Key = Variable)
  type solution = t Solution.dict

  exception Mismatch of t * t
  type problem = {pattern : t, term : t}

  structure Free = SplaySet(structure Elem = Variable)
  structure BoundPairs = SplaySet(structure Elem = DuplicateOrdered(Variable))

  fun merge (s1, s2) = raise Fail ""

  fun worker free pairs (pattern, term) sol =
    case (out pattern, out term) of
        (p \ pattern', q \ term') =>
        worker free (BoundPairs.insert pairs (p, q)) (pattern', term') sol
      | (oper $ args, oper' $ args') =>
        if Operator.eq (oper, oper')
        then Vector.foldl
               (fn (p, s) => worker free pairs p s)
               sol
               (VectorPair.zip (args, args'))
        else raise Mismatch (pattern, term)

  fun match {pattern, term} =
    worker
      (List.foldl (fn (v, s) => Free.insert s v) Free.empty (freeVariables pattern))
      BoundPairs.empty
      (pattern, term)
end
