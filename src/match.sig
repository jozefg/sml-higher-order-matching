signature HO_MATCH =
sig
  type t
  type var

  structure Solution : DICT where type key = var
  type solution = t Solution.dict

  type problem = {pattern : t, term : t}
  exception Mismatch of t * t

  val match : problem -> solution
end

signature ABT_APPLY =
sig
  include ABT

  val asInstantiate : t -> (t * t) option
  val instantiate   : t * t -> t
end
